﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiscMovement : MonoBehaviour
{
    public float discSpeed = 2f;            // Speed of the disc.
    int bounceCount = 0;                    // counts the amount of times the ball has touched a wall

    void Start()
    {
        //gets the rigidbody to move forward
        GetComponent<Rigidbody>().velocity = transform.forward * discSpeed;
    }

    //  when a dic hits a wall with the "wall" tag it adds +1 to the bounceCount
    private void OnCollisionEnter(Collision col)  //<- "col" is the name for our collision
    {
        if(col.gameObject.tag == "wall")
        { 
            bounceCount++;
        }
    }
   
    //  once the bounceCounts reaches 3 the ball gets destroyed
    private void FixedUpdate()
    {
        if (bounceCount == 3)
        {
            Destroy(gameObject);
        }
    }
}



